define k8s::master_systemd (
  String $k8s_version,
  )
{
  include ::systemd

  file { "/usr/lib/systemd/system/k8s-apiserver-${k8s_version}.service":
    ensure  => file,
    content => template('k8s/kube-apiserver.service.erb'),
    require => Class['k8s'],
  } ~>
  Exec['systemctl-daemon-reload']

  service { "k8s-apiserver-${k8s_version}":
    ensure  => running,
    enable  => true,
    require =>  [ File["/etc/kubernetes/${k8s_version}/config"], File["/etc/kubernetes/${k8s_version}/apiserver"], File["/usr/lib/systemd/system/k8s-apiserver-${k8s_version}.service"], File["/usr/bin/k8s-${k8s_version}/hyperkube"] ],
  }

  file { '/etc/kubernetes/kubeconfig-controller-manager':
    ensure  => file,
    content => epp('k8s/kubeconfig.epp', {'kubeconfig_user' => 'kube-controller-manager'}),
    require => File['/etc/kubernetes'],
  }

  file { "/usr/lib/systemd/system/k8s-controller-manager-${k8s_version}.service":
    ensure  => file,
    content => template('k8s/kube-controller-manager.service.erb'),
    require => Class['k8s'],
  } ~>
  Exec['systemctl-daemon-reload']

  service { "k8s-controller-manager-${k8s_version}":
    ensure  => running,
    enable  => true,
    require =>  [ File["/etc/kubernetes/${k8s_version}/config"], File["/usr/lib/systemd/system/k8s-controller-manager-${k8s_version}.service"], File["/usr/bin/k8s-${k8s_version}/hyperkube"], File['/etc/kubernetes/kubeconfig-controller-manager'], File["/etc/kubernetes/${k8s_version}/controller-manager"] ],
  }

  file { '/etc/kubernetes/kubeconfig-proxy':
    ensure  => file,
    content => epp('k8s/kubeconfig.epp', {'kubeconfig_user' => 'kube-proxy'}),
    require => File['/etc/kubernetes'],
  }

  file { "/etc/kubernetes/${k8s_version}/proxy":
    ensure  => file,
    content => 'KUBE_CONFIG="--kubeconfig=/etc/kubernetes/kubeconfig-proxy"',
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/usr/lib/systemd/system/k8s-proxy-${k8s_version}.service":
    ensure  => file,
    content => template('k8s/kube-proxy.service.erb'),
    require => Class['k8s'],
  } ~>
  Exec['systemctl-daemon-reload']

  service { "k8s-proxy-${k8s_version}":
    ensure  => running,
    enable  => true,
    require =>  [ File["/etc/kubernetes/${k8s_version}/config"], File["/usr/lib/systemd/system/k8s-proxy-${k8s_version}.service"], File["/usr/bin/k8s-${k8s_version}/hyperkube"], File['/etc/kubernetes/kubeconfig-proxy'], File["/etc/kubernetes/${k8s_version}/proxy"] ],
  }

  file { '/etc/kubernetes/kubeconfig-scheduler':
    ensure  => file,
    content => epp('k8s/kubeconfig.epp', {'kubeconfig_user' => 'kube-scheduler'}),
    require => File['/etc/kubernetes'],
  }

  file { "/etc/kubernetes/${k8s_version}/scheduler":
    ensure  => file,
    content => 'KUBE_CONFIG="--kubeconfig=/etc/kubernetes/kubeconfig-scheduler"',
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/usr/lib/systemd/system/k8s-scheduler-${k8s_version}.service":
    ensure  => file,
    content => template('k8s/kube-scheduler.service.erb'),
    require => Class['k8s'],
  } ~>
  Exec['systemctl-daemon-reload']

  service { "k8s-scheduler-${k8s_version}":
    ensure  => running,
    enable  => true,
    require =>  [ File["/etc/kubernetes/${k8s_version}/config"], File["/usr/lib/systemd/system/k8s-scheduler-${k8s_version}.service"], File["/usr/bin/k8s-${k8s_version}/hyperkube"], File['/etc/kubernetes/kubeconfig-scheduler'], File["/etc/kubernetes/${k8s_version}/scheduler"] ],
  }
}
