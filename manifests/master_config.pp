define k8s::master_config (
  String $k8s_version,
  String $allow_privileged,
  String $dns_root,
  Integer $k8s_etcd_port,
  Integer $etcd_count
  )
{
  file {  "/etc/kubernetes/${k8s_version}/config":
    ensure  => file,
    content => template('k8s/config.erb'),
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/etc/kubernetes/${k8s_version}/apiserver":
    ensure  => file,
    content => template('k8s/apiserver.erb'),
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/etc/kubernetes/${k8s_version}/controller-manager":
    ensure  => file,
    content => template('k8s/controller-manager.erb'),
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { '/etc/kubernetes/kubeconfig':
    ensure  => file,
    content => epp('k8s/kubeconfig.epp'),
    require => File['/etc/kubernetes'],
  }


  file { [ "/etc/kubernetes/${k8s_version}" ]:
    ensure  => directory,
    owner   => 'k8s',
    group   => 'k8s',
    require => Class['k8s'],
  }
}
