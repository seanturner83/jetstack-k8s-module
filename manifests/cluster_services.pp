class k8s::cluster_services {
  file { '/etc/kubernetes/addons':
    ensure => directory,
    mode   => '0750',
  }

  file { '/etc/kubernetes/addons/dashboard-controller.yaml':
    ensure  => file,
    content => template('k8s/dashboard-controller.yaml.erb'),
    require => File['/etc/kubernetes/addons'],
  } ->
  exec { 'Install dashboard controller':
    command => '/usr/bin/kubectl apply -f /etc/kubernetes/addons/dashboard-controller.yaml',
    unless  => '/usr/bin/kubectl get -f /etc/kubernetes/addons/dashboard-controller.yaml',
  }

  file { '/etc/kubernetes/addons/dashboard-service.yaml':
    ensure  => file,
    content => template('k8s/dashboard-service.yaml.erb'),
    require => File['/etc/kubernetes/addons'],
  } ->
  exec { 'Install dashboard service':
    command => '/usr/bin/kubectl apply -f /etc/kubernetes/addons/dashboard-service.yaml',
    unless  => '/usr/bin/kubectl get -f /etc/kubernetes/addons/dashboard-service.yaml',
  }

  file { '/etc/kubernetes/addons/dns-horizontal-autoscaler.yaml':
    ensure  => file,
    content => template('k8s/dns-horizontal-autoscaler.yaml.erb'),
    require => File['/etc/kubernetes/addons'],
  } ->
  exec { 'Install DNS autoscaler':
    command => '/usr/bin/kubectl apply -f /etc/kubernetes/addons/dns-horizontal-autoscaler.yaml',
    unless  => '/usr/bin/kubectl get -f /etc/kubernetes/addons/dns-horizontal-autoscaler.yaml',
  }

  file { '/etc/kubernetes/addons/kubedns-controller.yaml':
    ensure  => file,
    content => template('k8s/kubedns-controller.yaml.erb'),
    require => File['/etc/kubernetes/addons'],
  } ->
  exec { 'Install DNS controller':
    command => '/usr/bin/kubectl apply -f /etc/kubernetes/addons/kubedns-controller.yaml',
    unless  => '/usr/bin/kubectl get -f /etc/kubernetes/addons/kubedns-controller.yaml',
  }

  file { '/etc/kubernetes/addons/kubedns-svc.yaml':
    ensure  => file,
    content => template('k8s/kubedns-svc.yaml.erb'),
    require => File['/etc/kubernetes/addons'],
  } ->
  exec { 'Install DNS service':
    command => '/usr/bin/kubectl apply -f /etc/kubernetes/addons/kubedns-svc.yaml',
    unless  => '/usr/bin/kubectl get -f /etc/kubernetes/addons/kubedns-svc.yaml',
  }
}
