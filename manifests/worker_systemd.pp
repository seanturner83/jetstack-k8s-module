define k8s::worker_systemd (
  String $k8s_version,
  )
{
  include ::systemd

  file { '/etc/kubernetes/kubeconfig-kubelet':
    ensure  => file,
    content => epp('k8s/kubeconfig.epp', {'kubeconfig_user' => 'kubelet'}),
    require => File['/etc/kubernetes'],
  }

  file { "/usr/lib/systemd/system/k8s-kubelet-${k8s_version}.service":
    ensure  => file,
    content => template('k8s/kubelet.service.erb'),
    require => Class['k8s'],
  } ~>
  Exec['systemctl-daemon-reload']

  service { "k8s-kubelet-${k8s_version}":
    ensure  => running,
    enable  => true,
    require =>  [ File["/etc/kubernetes/${k8s_version}/config"], File["/etc/kubernetes/${k8s_version}/kubelet"], File["/usr/lib/systemd/system/k8s-kubelet-${k8s_version}.service"], File["/usr/bin/k8s-${k8s_version}/hyperkube"], File['/var/lib/kubelet'], Exec['/usr/bin/docker-storage-setup'], File['/etc/kubernetes/kubeconfig-kubelet'] ],
  }

  file { '/etc/kubernetes/kubeconfig-proxy':
    ensure  => file,
    content => epp('k8s/kubeconfig.epp', {'kubeconfig_user' => 'kube-proxy'}),
    require => File['/etc/kubernetes'],
  }

  file { "/etc/kubernetes/${k8s_version}/proxy":
    ensure  => file,
    content => 'KUBE_CONFIG="--kubeconfig=/etc/kubernetes/kubeconfig-proxy"',
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/usr/lib/systemd/system/k8s-proxy-${k8s_version}.service":
    ensure  => file,
    content => template('k8s/kube-proxy.service.erb'),
    require => Class['k8s'],
  } ~>
  Exec['systemctl-daemon-reload']

  service { "k8s-proxy-${k8s_version}":
    ensure  => running,
    enable  => true,
    require => [ File["/etc/kubernetes/${k8s_version}/config"], File["/usr/lib/systemd/system/k8s-proxy-${k8s_version}.service"], File["/usr/bin/k8s-${k8s_version}/hyperkube"], File['/etc/kubernetes/kubeconfig-proxy'], File["/etc/kubernetes/${k8s_version}/proxy"] ],
  }
}
