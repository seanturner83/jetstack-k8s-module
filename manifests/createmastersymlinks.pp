define k8s::createmastersymlinks (
  String $k8s_version,
  Boolean $include_proxy=true,
)
{
  file { [ '/usr/bin/apiserver', '/usr/bin/controller-manager', '/usr/bin/federation-apiserver', '/usr/bin/federation-controller-manager', '/usr/bin/kubectl', '/usr/bin/scheduler' ]:
    ensure => link,
    target => "/usr/bin/k8s-${k8s_version}/hyperkube",
  }

  if $include_proxy == true {
    file { '/usr/bin/proxy':
      ensure => link,
      target => "/usr/bin/k8s-${k8s_version}/hyperkube",
    }
  }
}
