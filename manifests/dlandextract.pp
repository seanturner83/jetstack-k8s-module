define k8s::dlandextract (
  String $k8s_version,
)
{
  wget::fetch { "download k8s version ${k8s_version}":
    source      => "https://storage.googleapis.com/kubernetes-release/release/v${k8s_version}/kubernetes-server-linux-amd64.tar.gz",
    destination => "/var/puppet/k8s-server-v${k8s_version}.tar.gz",
    before      => Exec["untar k8s version ${k8s_version}"],
  }

  file { "/var/puppet/k8s-server-v${k8s_version}":
    ensure => directory,
    mode   => '0755',
    before => Exec["untar k8s version ${k8s_version}"],
  }

  exec { "untar k8s version ${k8s_version}":
    command => "/bin/tar -xvzf /var/puppet/k8s-server-v${k8s_version}.tar.gz -C /var/puppet/k8s-server-v${k8s_version}",
    creates => "/var/puppet/k8s-server-v${k8s_version}/kubernetes/server/bin/hyperkube",
  }

  file { "/var/puppet/k8s-server-v${k8s_version}/kubernetes":
    ensure  => directory,
    mode    => '0755',
    recurse => true,
    require => Exec["untar k8s version ${k8s_version}"],
  }
}
