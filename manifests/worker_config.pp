define k8s::worker_config (
  String $k8s_version,
  String $allow_privileged,
  String $dns_root,
  String $docker_ebs_device,
  String $docker_vg_name,
  String $docker_vg_initial_size,
  String $network_plugin
  )
{
  file {  "/etc/kubernetes/${k8s_version}/config":
    ensure  => file,
    content => template('k8s/config.erb'),
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/etc/kubernetes/${k8s_version}/kubelet":
    ensure  => file,
    content => template('k8s/kubelet.erb'),
    require => File["/etc/kubernetes/${k8s_version}"],
  }

  file { "/etc/kubernetes/${k8s_version}":
    ensure  => directory,
    owner   => 'k8s',
    group   => 'k8s',
    require => Class['k8s'],
  }

  exec { 'semanage fcontext for /var/lib/kubelet':
    command => '/usr/sbin/semanage fcontext -a -t svirt_sandbox_file_t "/var/lib/kubelet(/.*)?"',
    unless  => '/usr/sbin/semanage fcontext -l | /usr/bin/grep "/var/lib/kubelet(/.*).*:svirt_sandbox_file_t"',
    require => File['/var/lib/kubelet'],
  }

  file { '/var/lib/kubelet':
    ensure  => directory,
    seltype => 'svirt_sandbox_file_t',
  }

  file { '/etc/sysconfig/docker-storage-setup':
    ensure  => file,
    content => template('k8s/docker-storage-setup.erb'),
    before  => Exec['/usr/bin/docker-storage-setup'],
    require => Package['docker'],
  }

  exec { '/usr/bin/docker-storage-setup':
    unless  => ['/usr/sbin/lvdisplay | grep docker-pool_tmeta', '/usr/sbin/lvdisplay | grep docker-pool_tdata'],
    require => File['/etc/sysconfig/docker-storage-setup'],
  }
}
