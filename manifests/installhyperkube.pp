define k8s::installhyperkube (
  String $k8s_version,
)
{
  file { "/usr/bin/k8s-${k8s_version}":
    ensure => directory,
    before => File["/usr/bin/k8s-${k8s_version}/hyperkube"],
  }

  file { "/usr/bin/k8s-${k8s_version}/hyperkube":
    ensure => file,
    mode   => '0755',
    # lint:ignore:puppet_url_without_modules
    source => "puppet:///binaries/k8s-server-v${k8s_version}/kubernetes/server/bin/hyperkube",
    # lint:endignore
  }
}
