define k8s::createworkersymlinks (
  String $k8s_version,
  )
{
  file { [ '/usr/bin/kubelet', '/usr/bin/proxy' ]:
    ensure => link,
    target => "/usr/bin/k8s-${k8s_version}/hyperkube",
  }
}
